INSERT INTO PRODUCT(id, name , description, illustration, price) values
(0, 'Produit id 0', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/820/300/300.jpg?hmac=pnsDU5kKsbiDU8RGNXdj224eHiGPRwtLIEIHWK0mQ60', 15),
(1, 'Produit id 1', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/844/300/300.jpg?hmac=cz7Fh4OjTE9Wv0TUWuua5rr-sqsA6-_NcVlD_SqcgS4', 16),
(2, 'Produit id 2', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/93/300/300.jpg?hmac=G-RoOLwpJz8chmE5nOLR0CEsZJIS83H9udgGgwanm0U', 45),
(3, 'Produit id 3', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/993/300/300.jpg?hmac=4GoIWE9T9XBxKLwUxVidze3r0k9sTybobEF2oRvjSLE', 9),
(4, 'Produit id 4', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/98/300/300.jpg?hmac=mx1lcO7kZSawi31-dbHSXhJtIR4Vt7iXi4vu6m50x8A', 150),
(5, 'Produit id 5', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/779/300/300.jpg?hmac=IxxDF6oRk6kCpreDU8fDpX-FG8cYqauZ6VHmtA6cSEU', 19),
(6, 'Produit id 6', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/480/300/300.jpg?hmac=SNeDbPA-87bxLtQ9W6l9Dy1QvsNoJhqR6IBDH5ACSHk', 32),
(7, 'Produit id 7', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/732/300/300.jpg?hmac=42BaZGQktL1uS0K3ymXISdqpPkD0UeEiToVbE5Wtdjk', 20),
(8, 'Produit id 8', 'Description qui donne envie d''acheter', 'https://i.picsum.photos/id/254/300/300.jpg?hmac=nfKffzQbf7WwMaj1aRn9DspXwVNOFx1tMOj7p3ONMjU', 49);
