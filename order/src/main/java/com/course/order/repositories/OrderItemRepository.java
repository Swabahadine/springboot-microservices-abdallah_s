package com.course.order.repositories;

import com.course.order.domain.OrderDomain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<OrderDomain, Long> {
}
