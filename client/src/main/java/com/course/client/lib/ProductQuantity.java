package com.course.client.lib;

import com.course.client.beans.ProductBean;

public class ProductQuantity {
    private long cartId;
    private ProductBean productBean;
    private int quantity;
    private Double totalPrice;

    public ProductQuantity(long id, ProductBean productBean, int quantity) {
        this.productBean = productBean;
        this.quantity = quantity;
        this.cartId = id;
    }

    public ProductQuantity(long cartId, ProductBean productBean, int quantity, Double totalPrice) {
        this.cartId = cartId;
        this.productBean = productBean;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public ProductBean getProductBean() {
        return productBean;
    }

    public void setProductBean(ProductBean productBean) {
        this.productBean = productBean;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }
}
