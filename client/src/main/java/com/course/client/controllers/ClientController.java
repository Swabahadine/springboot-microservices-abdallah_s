package com.course.client.controllers;

import com.course.client.beans.CartBean;
import com.course.client.beans.CartItemBean;
import com.course.client.beans.OrderBean;
import com.course.client.beans.ProductBean;
import com.course.client.lib.ProductQuantity;
import com.course.client.proxies.MsCartProxy;
import com.course.client.proxies.MsOrderProxy;
import com.course.client.proxies.MsProductProxy;
import com.course.client.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@Controller
public class ClientController {

    @Autowired
    private MsProductProxy msProductProxy;

    @Autowired
    private MsCartProxy msCartProxy;

    @Autowired
    private MsOrderProxy msOrderProxy;

    @Autowired
    private ClientService clientService;

    @RequestMapping("/")
    public String index(Model model) {

        List<ProductBean> products =  msProductProxy.list();
        System.out.println(products);

        model.addAttribute("products", products);

        return "index";
    }
    @RequestMapping("/product-detail/{id}")
    public String productDetail(@PathVariable Long id, Model model) {
        Optional<ProductBean> product =  msProductProxy.get(id);
        if (product.isPresent()) {
            model.addAttribute("product", product.get());
            System.out.println(product.get());
            return "product";
        }
        return "error/404";
    }

    //Cart
    @RequestMapping("/add-cart/{productId}")
    public String cartDetail(
            @PathVariable Long productId,
            @RequestParam int quantity,
            @CookieValue(value = "cart-current") Optional<Long> cartId,
            Model model,
            HttpServletResponse response) {
        Long newId = clientService.getCurrentCartId(cartId, response);
        System.out.println("productBean Id = " + productId);
        Optional<CartBean> cart =  msCartProxy.getCart(newId);
        if (cart.isPresent()) {
            CartItemBean cartItemBean = new CartItemBean();
            cartItemBean.setProductId(productId);
            cartItemBean.setQuantity(quantity);
            msCartProxy.addProductToCart(newId,cartItemBean);
            model.addAttribute("cart", cart.get());
            return "redirect:/mon-panier/";
        }
        return "error/404";
    }

    @RequestMapping("/mon-panier")
    public String cart(@CookieValue(value = "cart-current") Optional<Long> id, Model model, HttpServletResponse response) {
        Long newId = clientService.getCurrentCartId(id, response);
        System.out.println("New Id = " + newId);
        Optional<CartBean> cart =  msCartProxy.getCart(newId);
        if (cart.isPresent()) {
            CartBean cartBean = cart.get();
            try {
                List<ProductQuantity> productQuantities = clientService.convertCartToProductQuantity(cartBean.getProducts());

                model.addAttribute("productQuantities", productQuantities);
                model.addAttribute("totalPrice",clientService.totalPrice(productQuantities));
                System.out.println(productQuantities);
                return "cart";
            } catch (Exception e) {
                return "error/404";
            }
        }
        return "error/404";
    }

    @RequestMapping("/remove-cart-item/{idCartItem}")
    public String removeCartItem(
            @CookieValue(value = "cart-current") Optional<Long> id,
            @PathVariable Long idCartItem,HttpServletResponse response) {
        Long newId = clientService.getCurrentCartId(id, response);
        Optional<CartBean> cart =  msCartProxy.getCart(newId);
        if (cart.isPresent()) {
            System.out.println(""+ newId+ " | " + idCartItem);
            msCartProxy.removeProductToCart(newId, idCartItem);
            return "redirect:/mon-panier";
        }
        return "error/404";
    }


    @RequestMapping("/validation-commande")
    public String createOrder(
            @CookieValue(value = "cart-current") Optional<Long> cartId,
            Model model,
            HttpServletResponse response) {
        Long newCartId = clientService.getCurrentCartId(cartId, response);
        Optional<CartBean> optionalCart = msCartProxy.getCart(newCartId);
        if (optionalCart.isPresent()) {
            CartBean cartBean = optionalCart.get();
            try {
                System.out.println("try 1");
                OrderBean orderBean = clientService.convertCartToOrder(cartBean);
                System.out.println("try 2");
                msOrderProxy.createNewOrder(orderBean);
                System.out.println("try 3");
                // vider le panier
                cartBean.clearCart();
                System.out.println("try 4");
                msCartProxy.updateCart(cartBean);
                System.out.println("try 5");
            } catch (Exception e) {
                return "error/404";
            }
            return "redirect:/mes-commandes";
        }
        return "error/404";
    }

    @RequestMapping("/mes-commandes")
    public String myOrders(@CookieValue(value = "cart-current") Optional<Long> id, Model model, HttpServletResponse response) {
        Long cartId = clientService.getCurrentCartId(id, response);
        Optional<CartBean> cart =  msCartProxy.getCart(cartId);
        if (cart.isPresent()) {
            CartBean cartBean = cart.get();
            List<OrderBean> orderBeanList = msOrderProxy.getOrderByCartIdList(cartId);
            model.addAttribute("orderList", orderBeanList);
            return "order";
        }
        return "error/404";
    }

    @RequestMapping("/mes-commandes/{orderId}")
    public String cart(@PathVariable Long orderId, Model model, HttpServletResponse response) {
        Optional<OrderBean> orderBean =  msOrderProxy.getOrder(orderId);
        if (orderBean.isPresent()) {
            OrderBean order = orderBean.get();
            try {
                List<ProductQuantity> productQuantities = clientService.convertOrderToProductQuantity(order.getProducts());
                model.addAttribute("productQuantities", productQuantities);
                model.addAttribute("totalPrice",clientService.totalPrice(productQuantities));
                model.addAttribute("order",order);
                return "orderId";
            } catch (Exception e) {
                return "error/404";
            }
        }
        return "error/404";
    }
}
