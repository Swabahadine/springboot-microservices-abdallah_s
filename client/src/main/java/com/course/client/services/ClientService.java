package com.course.client.services;

import com.course.client.beans.*;
import com.course.client.lib.ProductQuantity;
import com.course.client.proxies.MsCartProxy;
import com.course.client.proxies.MsProductProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private MsProductProxy msProductProxy;

    @Autowired
    private MsCartProxy msCartProxy;

    public ProductBean getProductById(Long id) throws Exception {
        Optional<ProductBean> product =  msProductProxy.get(id);
        if (product.isPresent()) {
            return product.get();
        }
        throw new NullPointerException();
    }

    private Long checkCartIdCookie(Optional<Long> id, HttpServletResponse response){
        Long newId;
        if (!id.isPresent()) {
            // create a cookie
            ResponseEntity<CartBean> cartBean = msCartProxy.createNewCart(new CartBean());
            newId = cartBean.getBody().getId();
            Cookie cookie = new Cookie("cart-current", newId+"");
            response.addCookie(cookie);
        }else {
            newId = id.get();
        }
        return newId;
    }

    public Long getCurrentCartId(Optional<Long> oId, HttpServletResponse response){
        Long id = checkCartIdCookie(oId, response);
        Optional<CartBean> cartBean = msCartProxy.getCart(id);
        if (cartBean.isPresent()) {
            return id;
        }else {
            ResponseEntity<CartBean> newCart = msCartProxy.createNewCart(new CartBean());
            if (newCart.getStatusCode().is2xxSuccessful()){
                Long newId = newCart.getBody().getId();
                response.addCookie(new Cookie("cart-current", newId+""));
                return newId;
            }
            throw new NullPointerException();
        }
    }

    public List<ProductQuantity> convertCartToProductQuantity(List<CartItemBean> cartItemBeanList) throws Exception {
        List<ProductQuantity> productQuantities = new ArrayList<>();
        for (CartItemBean cartItemBean: cartItemBeanList){
            Long id = cartItemBean.getId();
            Long productId = cartItemBean.getProductId();
            ProductBean productBean = this.getProductById(productId);
            int quantity = cartItemBean.getQuantity();
            double totalPrice = productBean.getPrice() * quantity;
            productQuantities.add(new ProductQuantity(id, productBean, quantity, totalPrice));
        }
        return productQuantities;
    }

    public List<ProductQuantity> convertOrderToProductQuantity (List<OrderItemBean> cartItemBeanList) throws Exception {
        List<ProductQuantity> productQuantities = new ArrayList<>();
        for (OrderItemBean cartItemBean: cartItemBeanList){
            Long id = cartItemBean.getId();
            Long productId = cartItemBean.getProductId();
            ProductBean productBean = this.getProductById(productId);
            int quantity = cartItemBean.getQuantity();
            double totalPrice = productBean.getPrice() * quantity;
            productQuantities.add(new ProductQuantity(id, productBean, quantity, totalPrice));
        }
        return productQuantities;
    }

    public Double totalPrice(List<ProductQuantity> productQuantities){
        Double m = 0.0;
        for(ProductQuantity p: productQuantities){
            m += p.getTotalPrice();
        }
        return m;
    }

    public List<OrderItemBean> convertListCartItemToListOrderItem (List<CartItemBean> cartItemBeanList) throws Exception {
        List<OrderItemBean> orderItemBeans = new ArrayList<>();
        List<ProductQuantity> productQuantities = convertCartToProductQuantity(cartItemBeanList);
        for (ProductQuantity productQuantity: productQuantities){
            Long productId = productQuantity.getProductBean().getId();
            Integer quantity = productQuantity.getQuantity();
            Double totalPrice = productQuantity.getTotalPrice();
            orderItemBeans.add(new OrderItemBean(productId, quantity, totalPrice));
        }
        return orderItemBeans;
    }

    public OrderBean convertCartToOrder (CartBean cartBean) throws Exception {
        List<OrderItemBean> products = convertListCartItemToListOrderItem(cartBean.getProducts());
        Double totalPrice = 0.0;
        Long cartId = cartBean.getId();
        for (OrderItemBean orderItemBean: products){
            totalPrice += orderItemBean.getTotalPrice();
        }
        return new OrderBean(
                products,
                totalPrice,
                cartId
        );
    }
}
